/////////////////////////////////////////////////////////////////////////
// main.cpp                                                            //
//                                                                     //
// Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>//
// Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>//
//                                                                     //
// This library is free software; you can redistribute it and/or       //
// modify it under the terms of the GNU Lesser General Public          //
// License as published by the Free Software Foundation; either        //
// version 2.1 of the License, or (at your option) any later version.  //
//                                                                     //
// This library is distributed in the hope that it will be useful,     //
// but WITHOUT ANY WARRANTY; without even the implied warranty of      //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   //
// Lesser General Public License for more details.                     //
//                                                                     //
// You should have received a copy of the GNU Lesser General Public    //
// License along with this library; if not, write to the Free Software //
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       //
// 02110-1301  USA                                                     //
/////////////////////////////////////////////////////////////////////////
#include <QtGui>
#include "item.h"
#include "kineticview.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QGraphicsScene scene;
    scene.setSceneRect(0, 0, 600, 600);

    AItem ui[30];
    KineticView *t = new KineticView;

    scene.addItem( t );
    QGraphicsWidget *w = new QGraphicsWidget;
    QGraphicsLinearLayout *l = new QGraphicsLinearLayout( Qt::Vertical, w );

    ui[0].setColor( Qt::black );
    ui[1].setColor( Qt::green );
    ui[2].setColor( Qt::red );
    ui[3].setColor( Qt::blue );
    ui[4].setColor( Qt::gray );
    ui[5].setColor( Qt::lightGray );
    ui[6].setColor( Qt::yellow );
    ui[7].setColor( Qt::black );
    ui[8].setColor( Qt::green );
    ui[9].setColor( Qt::red );
    ui[10].setColor( Qt::black );
    ui[11].setColor( Qt::green );
    ui[12].setColor( Qt::red );
    ui[13].setColor( Qt::blue );
    ui[14].setColor( Qt::gray );
    ui[15].setColor( Qt::lightGray );
    ui[16].setColor( Qt::yellow );
    ui[17].setColor( Qt::black );
    ui[18].setColor( Qt::green );
    ui[19].setColor( Qt::red );
    ui[20].setColor( Qt::black );
    ui[21].setColor( Qt::green );
    ui[22].setColor( Qt::red );
    ui[23].setColor( Qt::blue );
    ui[24].setColor( Qt::gray );
    ui[25].setColor( Qt::lightGray );
    ui[26].setColor( Qt::yellow );
    ui[27].setColor( Qt::black );
    ui[28].setColor( Qt::green );
    ui[29].setColor( Qt::red );


    for( int i = 0 ; i < 30; i++ ) {
        l->addItem( &ui[i] );
    }
    t->setWidget( w );
    t->resize( 300, 600);

    QGraphicsView view(&scene);
    view.setRenderHint(QPainter::Antialiasing);
    /* view drag */
    //view.setDragMode(QGraphicsView::ScrollHandDrag);
    view.show();

    return app.exec();
}
