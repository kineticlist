#include "scrollbar.h"

#include <QScrollBar>

ScrollBar::ScrollBar( QGraphicsItem *parent )
    : QGraphicsProxyWidget( parent )
{
    QScrollBar *scrollbar = new QScrollBar;

    connect( scrollbar, SIGNAL( valueChanged( int ) ), this, SLOT( setValueChanged( int ) ) );

    setWidget( scrollbar );
}

ScrollBar::~ScrollBar()
{
}

void ScrollBar::setValueChanged( int value )
{
    emit valueChanged( value );
}

void ScrollBar::setMaximum( int max )
{
    QScrollBar *scrollbar = static_cast<QScrollBar *>( widget() );
    scrollbar->setMaximum( max );
}

void ScrollBar::setValue( int value )
{
    QScrollBar *scrollbar = static_cast<QScrollBar *>( widget() );
    scrollbar->setValue( -value );
}
