#ifndef SCROLLBAR_H
#define SCROLLBAR_H

#include <QGraphicsProxyWidget>

class ScrollBar : public QGraphicsProxyWidget
{
    Q_OBJECT

    public:
        ScrollBar( QGraphicsItem *parent );
        ~ScrollBar();

        void setMaximum( int max );
        void setValue( int value );
    Q_SIGNALS:
        void valueChanged( int value );

    public Q_SLOTS:
        void setValueChanged( int value );
};

#endif
