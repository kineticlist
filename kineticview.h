/***********************************************************************/
/* kineticview.h 						       */
/* 								       */
/* Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>*/
/* Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>*/
/* 								       */
/* This library is free software; you can redistribute it and/or       */
/* modify it under the terms of the GNU Lesser General Public	       */
/* License as published by the Free Software Foundation; either	       */
/* version 2.1 of the License, or (at your option) any later version.  */
/*   								       */
/* This library is distributed in the hope that it will be useful,     */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   */
/* Lesser General Public License for more details.		       */
/*  								       */
/* You should have received a copy of the GNU Lesser General Public    */
/* License along with this library; if not, write to the Free Software */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       */
/* 02110-1301  USA						       */
/***********************************************************************/
#ifndef KINETICLAYOUT_H
#define KINETICLAYOUT_H

#include <QGraphicsWidget>
#include <QGraphicsSceneMouseEvent>

class QGraphicsGridLayout;
class ScrollBar;
class KineticScrollingPrivate;
class QPropertyAnimation;

class KineticView : public QGraphicsWidget
{
    Q_OBJECT

    public:
        KineticView(QGraphicsWidget *parent = 0);
        ~KineticView();

        void setWidget(QGraphicsWidget *item);

    private:

        QGraphicsGridLayout *layout;
        ScrollBar *verticalScrollbar;
        KineticScrollingPrivate *d;
        QPropertyAnimation *scrollAnimation;

    protected:
        void resizeEvent( QGraphicsSceneResizeEvent *event );
        bool eventFilter( QObject *watched, QEvent *event );

        void mousePressEvent(QGraphicsSceneMouseEvent *event);
        void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
        void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
        void wheelEvent(QGraphicsSceneWheelEvent *event);

        int thresholdPosition( int value );
        void resetAnimation( int finalPosition, int duration );

    public Q_SLOTS:
        void setVerticalScrollValue(int value);
        void overshoot();

    private:
        void adjustScrollBar();
};

#endif
