/////////////////////////////////////////////////////////////////////////
// kineticview.cpp                                                     //
//                                                                     //
// Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>//
// Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>//
//                                                                     //
// This library is free software; you can redistribute it and/or       //
// modify it under the terms of the GNU Lesser General Public          //
// License as published by the Free Software Foundation; either        //
// version 2.1 of the License, or (at your option) any later version.  //
//                                                                     //
// This library is distributed in the hope that it will be useful,     //
// but WITHOUT ANY WARRANTY; without even the implied warranty of      //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   //
// Lesser General Public License for more details.                     //
//                                                                     //
// You should have received a copy of the GNU Lesser General Public    //
// License along with this library; if not, write to the Free Software //
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       //
// 02110-1301  USA                                                     //
/////////////////////////////////////////////////////////////////////////
#include "kineticview.h"
#include "scrollbar.h"

#include <QObject>
#include <QDebug>
#include <QGraphicsGridLayout>
#include <QScrollBar>
#include <QPropertyAnimation>
#include <QTime>

/* TODO:
 * - fix velocity( create a new easing curve? )
 * - kinetic velocity parameterized upon count of list items
 * - horizontal scrolling
 * - parameterize dimensions
 * - ringbuffer (minimize number of items in the QGV).
 */

class KineticScrollingPrivate
{
public:
    KineticScrollingPrivate(): mScrollVelocity(0), timerID(0),
                               overshoot(40), bounceFlag(0), bounceStatus( Finished )
    { }

    void count()
    {
        t = QTime::currentTime();
    }

    unsigned int elapsed()
    {
        return t.msecsTo( QTime::currentTime() );
    }

    void verticalScroll(int value)
    {
        widget->setPos(QPoint(0, -value*10));
    }

    void horizontalScroll(int value)
    {
        widget->setPos(QPoint(-value*10, 0));
    }

    /* Just for backport sake */
    int normalMovement()
    {
        return movement;
    }

    int kinMovement()
    {
        return kin_movement;
    }

    qreal duration()
    {
        return timeDelta;
    }

    void mousePressEvent(QGraphicsSceneMouseEvent *event)
    {
        Q_UNUSED(event);
        count();
        scrollVelocity = 0;
        movement = 0;
        kin_movement = 0;
    }

    void mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        int temp = event->lastPos().y() - event->pos().y();
        if (temp) {
            movement = temp;
            kin_movement += temp;
        }
    }

    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
    {
        timeDelta = elapsed();
        int temp = event->lastPos().y() - event->pos().y();
        if (temp)
            kin_movement += temp;

        if (timeDelta > 200) {
            if (kin_movement > 0)
                kin_movement = 3;
            else
                kin_movement = -3;
        }
    }

    void wheelReleaseEvent(QGraphicsSceneWheelEvent *event)
    {
        timeDelta = elapsed();
        int temp = event->delta();
        temp *= -1;
        kin_movement += temp;
    }
    /* Just for backport sake */

    unsigned int timeDelta;
    qreal scrollVelocity;
    int movement;
    int kin_movement;

    qreal mScrollVelocity;
    enum { None, Up, Down };
    enum BounceStatus{ Running, Finished };
    int timerID, overshoot, cposition, direction, minimalPos, maximumPos;
    char bounceFlag;
    BounceStatus bounceStatus;

    QGraphicsWidget *widget;
    QGraphicsWidget *scrollingWidget;

protected:
    QTime t;
};

KineticView::KineticView(QGraphicsWidget *parent)
    : QGraphicsWidget(parent)
{
    d = new KineticScrollingPrivate;
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    d->scrollingWidget = new QGraphicsWidget(this);
    d->scrollingWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    d->scrollingWidget->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
    layout = new QGraphicsGridLayout(this);
    layout->addItem(d->scrollingWidget, 0, 0);

    verticalScrollbar = new ScrollBar(this);
    connect(verticalScrollbar, SIGNAL(valueChanged(int)), this, SLOT(setVerticalScrollValue(int)));
    layout->addItem(verticalScrollbar, 0, 1);

    setAcceptedMouseButtons(Qt::LeftButton);
}

KineticView::~KineticView()
{
    delete d->widget;
    delete d;
    delete scrollAnimation;
}

void KineticView::adjustScrollBar()
{
    verticalScrollbar->setMaximum( qMax( 0,
                int( d->widget->size().height() - d->scrollingWidget->size().height())));
}

void KineticView::setWidget(QGraphicsWidget *item)
{
    d->widget = item;
    d->widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    d->widget->setParentItem(d->scrollingWidget);
    d->widget->setPos(0, 0);
    d->widget->setAcceptedMouseButtons(Qt::LeftButton);

    scrollAnimation = new QPropertyAnimation(d->widget, "geometry");
    connect(scrollAnimation, SIGNAL(finished()), this, SLOT(overshoot()));
    scrollAnimation->setEasingCurve(QEasingCurve::OutCirc);

    d->widget->installEventFilter( this );
    adjustScrollBar();
    update();
}

void KineticView::overshoot()
{
    /* Detect if bouncer */
    if( d->bounceStatus != KineticScrollingPrivate::Running ) {
        if( ( d->cposition > 0 ) || ( d->cposition < d->minimalPos + d->overshoot ) ) {
            int finalPosition;
            scrollAnimation->setEasingCurve( QEasingCurve::OutBounce );

            if (d->cposition > 0)
                finalPosition = 0;
            else
                finalPosition = -d->widget->size().height( ) + d->scrollingWidget->size().height();

            resetAnimation( finalPosition, 900 );
            d->bounceStatus = KineticScrollingPrivate::Running;
        }
    } else {
        d->bounceStatus = KineticScrollingPrivate::Finished;
        scrollAnimation->setEasingCurve( QEasingCurve::OutCirc );
    }

}

void KineticView::setVerticalScrollValue(int value)
{
    const int pos = thresholdPosition( -value );
    d->widget->setPos(0, pos);

    if( ( pos == d->overshoot ) || ( pos == d->minimalPos ) )
        overshoot();
}

int KineticView::thresholdPosition(int value)
{

     d->minimalPos = -d->widget->size().height() + d->scrollingWidget->size().height()
         -d->overshoot;
     d->minimalPos = qMin(d->overshoot, d->minimalPos);
     d->maximumPos = value;

     d->cposition = qBound(d->minimalPos, d->maximumPos, d->overshoot);

     return d->cposition;
}

void KineticView::resetAnimation(int finalPosition, int duration)
{
    if (scrollAnimation->state() != QAbstractAnimation::Stopped)
        scrollAnimation->stop();
    d->cposition = finalPosition;
    QRectF tmpGeometry = d->widget->geometry();
    scrollAnimation->setStartValue(tmpGeometry);
    tmpGeometry.setY(d->cposition);
    scrollAnimation->setEndValue(tmpGeometry);
    scrollAnimation->setDuration(duration);
    scrollAnimation->start();

}


void KineticView::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (scrollAnimation->state() != QAbstractAnimation::Stopped)
        scrollAnimation->stop();

    event->accept();
    scrollAnimation->setEasingCurve(QEasingCurve::OutCirc);
    d->mousePressEvent(event);
}

void KineticView::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    d->mouseMoveEvent(event);
    setVerticalScrollValue(-(d->widget->pos().y() - d->normalMovement()));
}

void KineticView::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    if( scrollAnimation->state() != QAbstractAnimation::Running ) {
        d->mouseReleaseEvent(event);

        const int finalPos = thresholdPosition(d->widget->pos().y() - d->kinMovement()*6);
        resetAnimation( finalPos, d->duration()*8 );
    }
}

void KineticView::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    event->accept();
    scrollAnimation->setEasingCurve( QEasingCurve::OutCirc );
    d->mousePressEvent(NULL);
    d->wheelReleaseEvent(event);
    const int finalPos = thresholdPosition(d->widget->pos().y() - d->kinMovement()*6);
    resetAnimation( finalPos, 900 );
}

void KineticView::resizeEvent(QGraphicsSceneResizeEvent *event)
{
    if (d->widget)
        adjustScrollBar();
    QGraphicsWidget::resizeEvent(event);
}

bool KineticView::eventFilter(QObject *watched, QEvent *event)
{
    if (!d->widget) {
        return false;
    }

    if (watched == d->widget && event->type() == QEvent::GraphicsSceneMove) {
        verticalScrollbar->blockSignals(true);
        verticalScrollbar->setValue(d->widget->pos().y());
        verticalScrollbar->blockSignals(false);
    }

    return false;
}
