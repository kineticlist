/////////////////////////////////////////////////////////////////////////
// item.cpp                                                            //
//                                                                     //
// Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>//
//                                                                     //
// This library is free software; you can redistribute it and/or       //
// modify it under the terms of the GNU Lesser General Public          //
// License as published by the Free Software Foundation; either        //
// version 2.1 of the License, or (at your option) any later version.  //
//                                                                     //
// This library is distributed in the hope that it will be useful,     //
// but WITHOUT ANY WARRANTY; without even the implied warranty of      //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   //
// Lesser General Public License for more details.                     //
//                                                                     //
// You should have received a copy of the GNU Lesser General Public    //
// License along with this library; if not, write to the Free Software //
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       //
// 02110-1301  USA                                                     //
/////////////////////////////////////////////////////////////////////////
#include "item.h"
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

AItem::AItem( QGraphicsWidget *parent): QGraphicsWidget( parent )
{
    setAcceptedMouseButtons( Qt::LeftButton );
    QRectF tmp( 0, 0, 250, 100 );
    setGeometry( tmp );
    color = Qt::gray;
    //setAcceptedMouseButtons( Qt::LeftButton );
}

AItem::AItem( AItem &item )
{
    setAcceptedMouseButtons( Qt::LeftButton );
    setGeometry( item.geometry() );
    setPos( 0, 0 );
}

void AItem::setColor( QColor color )
{
    this->color = color;
}

QSizeF AItem::sizeHint(Qt::SizeHint which, const QSizeF & constraint) const
{
    Q_UNUSED( which );
    Q_UNUSED( constraint );
    return QSizeF( 250, 100 );
}


void AItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                  QWidget *widget)
{
    Q_UNUSED( widget );
    Q_UNUSED( option );

    painter->setBrush( color );
    painter->drawRoundedRect( 0, 0, geometry().width(),
                              geometry().height(), 20.0, 15.0 );

}
void AItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug() << "item clicked!";
    return QGraphicsItem::mousePressEvent(event);
}
